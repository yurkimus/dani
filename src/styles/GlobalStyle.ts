import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  body {
    font-family: "Architects Daughter", cursive;

    overflow-x: hidden;
  }
  
  ul {
    list-style: none;
  }
  
  button {
    cursor: pointer;
  }
`
