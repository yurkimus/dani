const color = {
  main: 'cornflowerblue',
  secondary: 'darkslateblue',
  accent: '#ffd400',
  btnLink: 'white',
  link: 'white',
  text: 'black'
}

const borderRadius = '10px'

export const theme = {
  color,
  borderRadius
}
