import React, { FC } from 'react'
import styled from 'styled-components'
import { config, useTransition } from 'react-spring'

const Wrapper = styled.div`
  margin-bottom: 1rem;

  display: grid;
  grid-gap: 0.75rem 1rem;
  grid-template-columns: repeat(3, 1fr);

  :last-child {
    margin-bottom: 0;
  }

  @media (max-width: 900px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 640px) {
    grid-template-columns: repeat(1, 1fr);
  }
`

export interface IListComponentProps<Data, CardProps> {
  data: Data
  card: React.FC<CardProps>
}

export const List: FC<IListComponentProps<any, any>> = ({
  data,
  card: Card
}) => {
  const transitions = useTransition(data, (item) => item.id, {
    from: { opacity: 0, transform: 'translateX(100%) rotate(-25deg)' },
    enter: [
      { opacity: 1, transform: 'translateX(0) rotate(0deg)' },
      { opacity: 1, transform: 'translateX(0) rotate(0deg)' }
    ],
    leave: { opacity: 0, transform: 'translateX(50%)' },
    config: config.stiff,
    trail: 100
  })

  return (
    <Wrapper>
      {data &&
        transitions.map(({ item, props, key }) => (
          <Card data={item} style={props} key={key} />
        ))}
    </Wrapper>
  )
}
