import React, { FC } from 'react'
import styled from 'styled-components'
import { Link } from './Link'
import { animated } from 'react-spring'

const Wrapper = styled(animated.figure)`
  padding: 0.75rem;

  background-color: ${(p) => p.theme.color.link};
  border-radius: ${(p) => p.theme.borderRadius};
  border: 2px solid ${(p) => p.theme.color.secondary};
  box-shadow: 0 0 2px ${(p) => p.theme.color.secondary};

  display: flex;
  flex-flow: column nowrap;
`

const Image = styled.img`
  margin-bottom: 1rem;

  width: 100%;
  max-width: 100%;
  min-height: 160px;
  height: 100%;
  max-height: 160px;

  border-radius: ${(p) => p.theme.borderRadius};
  background-color: ${(p) => p.theme.color.secondary};
  object-fit: cover;
  border: 2px solid ${(p) => p.theme.color.accent};
  box-shadow: 0 0 2px ${(p) => p.theme.color.accent};

  @media (max-width: 900px) {
    margin-bottom: 0.5rem;
  }
`

const Headline = styled.h2`
  font-size: 1.2rem;

  @media (max-width: 900px) {
    font-size: 1rem;
  }
`

const Description = styled.figcaption``

const Text = styled.p`
  font-size: 1rem;

  @media (max-width: 900px) {
    font-size: 0.8rem;
  }
`

interface IComponentProps<Data> {
  data: Data
  style?: React.CSSProperties
}

export const Card: FC<IComponentProps<any>> = ({ data, style }) => {
  return (
    <Wrapper style={style}>
      <Image src="https://i.ytimg.com/vi/e9eypZCGEZQ/hqdefault.jpg?sqp=-oaymwEjCPYBEIoBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLDhq4Hv4gWLRjgfFzBvhF29kQ6SJg" alt="" />

      <Headline children="Headline!" />

      <Description>
        <Text children="Text!" />
      </Description>

      <Link url="" />
    </Wrapper>
  )
}
