import React, { FC } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { routes } from '../../routes/routes'

const FooterWrapper = styled.footer`
  padding: 1rem 2rem;

  background-color: ${(p) => p.theme.color.secondary};
  border-radius: ${(p) => p.theme.borderRadius};

  position: fixed;
  bottom: 0;
  right: 0;
  z-index: 10;
  transform: translate(-12.5%, -25%);

  @media (max-width: 900px) {
    padding: 0.5rem 1rem;

    font-size: 0.9rem;
  }
`

const LinksWrapper = styled.nav`
  ul {
    height: 100%;
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;

    li {
      margin-bottom: 1rem;
      display: flex;
      justify-content: center;
      align-items: center;

      :last-child {
        margin-bottom: 0;
      }
    }
  }

  a {
    color: aliceblue;
    text-decoration: none;

    :hover {
      color: cornflowerblue;
    }
  }
`

export const Footer: FC = () => {
  return (
    <FooterWrapper>
      <LinksWrapper>
        <ul>
          <li>
            <Link to={routes.contact} children="Contacts" />
          </li>
        </ul>
      </LinksWrapper>
    </FooterWrapper>
  )
}
