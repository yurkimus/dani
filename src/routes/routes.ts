export const routes = {
  home: '/',
  games: '/games',
  videos: '/videos',
  contact: '/contact'
}
