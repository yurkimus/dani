import React, { FC } from 'react'
import styled from 'styled-components'
import { List } from '../components/common/List'
import { VideoCard } from '../components/Card/VideoCard'

const Wrapper = styled.div``

const data = [
  { id: 'sdg32ly' },
  { id: 'lhleoe2' },
  { id: 'sdqwprt' },
  { id: 'pdsgps0' },
  { id: 'qlmbcx3' },
  { id: 'qlmbcx5' },
  { id: 'qlmbcx4' },
  { id: 'qlmbcx6' }
]

interface IComponentProps {}

export const Videos: FC<IComponentProps> = () => {
  return (
    <Wrapper>
      <h1>Videos!</h1>

      <List data={data} card={VideoCard} />
    </Wrapper>
  )
}
