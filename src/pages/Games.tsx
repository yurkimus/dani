import React, { FC } from 'react'
import styled from 'styled-components'
import { List } from '../components/common/List'
import { GameCard } from '../components/Card/GameCard'

const Wrapper = styled.div``

const data = [
  { id: 'sdg32ly' },
  { id: 'lhleoe2' },
  { id: 'sdqwprt' },
  { id: 'pdsgps0' },
  { id: 'qlmbcx3' }
]

interface IComponentProps {}

export const Games: FC<IComponentProps> = () => {
  return (
    <Wrapper>
      <h1>Games!</h1>

      <List data={data} card={GameCard} />
    </Wrapper>
  )
}
